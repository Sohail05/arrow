function inRange(n, range) {
    return (n >= range.a) && (n <= range.b);
}


//graph object
class Graph {
    constructor() {
        this.nodes = [];
        this.screen = {
            width: 500,
            height: 500
        };
    }

    addNode(value = "New Node") {
        this.nodes.push(new Node(value));
    }

    insertNode(node = new Node()) {
        this.nodes.push(node);
    }
}

class Node {

    constructor(value = "New Node") {
        console.log(value);

        this.position = {
            x: 100,
            y: 100
        };
        this.scale = {
            x: 200,
            y: 200
        };
        this.value = value;
        this.pins = { in: [],
            out: [1]
        };

        if (typeof value == typeof alert) {
            this.pins.in.push(...
                "1".repeat(value.length));
        }
        this.entity = "";
    }

    inDomain(coordonates) {
        return inRange(coordonates.x, {
                a: this.position.x,
                b: this.position.x+this.scale.x
            }) &&
            inRange(coordonates.y, {
                a: this.position.y,
                b: this.position.y+this.scale.y
            });
    }

    //createDrawSetting
    draw(renderer) {
        if(!this.entity){
          this.entity = renderer.makeRoundedRectangle(this.position.x, this.position.y, this.scale.x, this.scale.y, 10);
           //this.entity.stroke = "black";
        }
      //  this.entity.translation.set(this.position.x, this.position.y);
        //rect.linewidth = "5";
        //rect.opacity = 0.5
    }

}

var app = new Vue({

    el: "#app",
    data: {
        graph: new Graph(),
        parameters: [],
        dragging: false,
        mousePressed: false,
        mouse: {
            x: 0,
            y: 0
        },

    },
    ready() {

        var params = {
            type: Two.Types.canvas,
            width: this.graph.width,
            heigh: this.graph.height
        };
        var two = new Two(params).appendTo(this.$el);

        this.graph.addNode();

        // Don't forget to tell two to render everything
        // to the screen
        var draw = () => {
            requestAnimationFrame(draw);
            this.graph.nodes.forEach((n) => {
                n.draw(two);
            });
            //two.makeCircle(this.$data.mouse.x,10, 5);
            two.update();
        };

        draw();
    },
    watch: {},
    computed: {},
    components: {
        node: {
            data: "",
            props: ["node"],
            template: "#node-template",
            methods: {}
        }
    },
    methods: {
        addNode() {
            this.graph.addNode();
        },
        mouseup() {
            this.mousePressed = false;
            this.dragging = false;
        },
        mousedown(e) {
            this.mousePressed = true;
            this.select({
                x: e.offsetX ,
                y: e.offsetY
            });
        },
        mousemove(e) {
            this.mouse = {
               x:  e.offsetX  ,
               y: e.offsetY
            };
        },
        select(coordonates) {

            this.graph.nodes.forEach((x) => {
                console.log(x.inDomain(coordonates));
            });
        }

    }

});
